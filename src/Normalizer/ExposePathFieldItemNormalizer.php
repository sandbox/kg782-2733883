<?php

namespace Drupal\expose_path\Normalizer;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Path\AliasStorageInterface;
use Drupal\path\Plugin\Field\FieldType\PathFieldItemList;
use Drupal\Component\Utility\NestedArray;

/**
 * Provides a normalizer for path fields.
 *
 * The generic field normalizer is useless for path fields because they do not
 * contain any field items upon loading, so they are always skipped. In contrast
 * this normalizer checks whether an alias exists for the entity's path and, if
 * so, creates a field item containing the alias dynamically. That way the alias
 * will end up in the normalized entity. If the entity is then denormalized and
 * saved, an alias will get created by PathItem::postSave(). Note that because
 * path aliases do not support UUIDs we do not normalize the alias' identifier
 * ("pid").
 *
 * @see \Drupal\hal\Normalizer\FieldNormalizer::normalizeFieldItems()
 * @see \Drupal\path\Plugin\Field\FieldType\PathItem::postSave()
 */
class ExposePathFieldItemNormalizer extends NormalizerBase {


  /**
   * The path alias storage.
   *
   * @var \Drupal\Core\Path\AliasStorageInterface
   */
  protected $aliasStorage;

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = PathFieldItemList::class;

  /**
   * Constructs a PathFieldNormalizer object.
   * .
   * @param \Drupal\Core\Path\AliasStorageInterface $alias_storage
   *   The path alias storage.
   */
  public function __construct() {
    $this->aliasStorage = \Drupal::service('path.alias_storage');
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($field, $format = NULL, array $context = array()) {
    $normalized_field_items = array();

    // Get the field definition.
    $entity = $field->getEntity();
    $field_name = $field->getName();
    $field_definition = $field->getFieldDefinition();

    // If this field is not translatable, it can simply be normalized without
    // separating it into different translations.
    if (!$field_definition->isTranslatable()) {
      $normalized_field_items = $this->normalizeFieldItems($field, $format, $context);
    }
    // Otherwise, the languages have to be extracted from the entity and passed
    // in to the field item normalizer in the context. The langcode is appended
    // to the field item values.
    else {
      foreach ($entity->getTranslationLanguages() as $language) {
        $context['langcode'] = $language->getId();
        $translation = $entity->getTranslation($language->getId());
        $translated_field = $translation->get($field_name);
        $normalized_field_items = array_merge($normalized_field_items, $this->normalizeFieldItems($translated_field, $format, $context));
      }
    }

    // Merge deep so that links set in entity reference normalizers are merged
    // into the links property.
    $normalized = NestedArray::mergeDeepArray($normalized_field_items);
    return $normalized;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItems($field, $format, $context) {
    /** @var \Drupal\Core\Field\FieldItemListInterface $field */
    $normalized_field_items = [];

    $entity = $field->getEntity();
    /** @see \Drupal\path\Plugin\Field\FieldWidget\PathWidget::formElement() */
    if (!$entity->isNew()) {
      $conditions = ['source' => '/' . $entity->toUrl()->getInternalPath()];
      if (isset($context['langcode']) && ($context['langcode'] != LanguageInterface::LANGCODE_NOT_SPECIFIED)) {
        $conditions['langcode'] = $context['langcode'];
      }
      $path = $this->aliasStorage->load($conditions);
      if ($path !== FALSE) {
        $field_item = $field->appendItem(['alias' => $path['alias']]);
        $normalized_field_items[] = $this->serializer->normalize($field_item, $format, $context);
      }
    }

    return $normalized_field_items;
  }

}